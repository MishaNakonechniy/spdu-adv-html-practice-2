import {goTo} from '/js/utils';
import {getCardTemplate} from '/js/templates';
import {fetchCharactersData} from '/js/fetch';

export const displayCharactersData = rootElement => {
    let queries = {
        limit: 20,
        page: 0,
        nameStartsWith: ''
    };

    const setCharactersQueries = nextQueries => {
        queries = {...queries, ...nextQueries};
    };

    const resetCharacters = () => {
        queries.page = 0;
        rootElement.innerHTML = '';
    };

    const loadCharacters = () => {
        fetchCharactersData(queries).then(response => {
            const html = response.data.results.map(({id, name, description, thumbnail}) => (
                getCardTemplate({
                    id,
                    name,
                    description,
                    thumbnail: `${thumbnail.path}.${thumbnail.extension}`,
                    onClick: (event, id) => {
                        goTo(`/${id}`)
                    }
                })
            ));

            rootElement.append(...html);

            queries.page += 1;
        });
    };

    return {
        setCharactersQueries,
        loadCharacters,
        resetCharacters
    };
};
