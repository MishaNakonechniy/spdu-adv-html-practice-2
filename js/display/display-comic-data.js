import {getSinglePageTemplate} from '/js/templates';
import {fetchComicData} from '/js/fetch';

export const displayComicData = rootElement => {
    const resetComic = () => {
        rootElement.innerHTML = '';
    };

    const loadComic = id => {
        fetchComicData(id).then(response => {
            const html = response.data.results.map(({id, title, description, thumbnail}) => (
                getSinglePageTemplate({
                    id,
                    name: title,
                    description,
                    thumbnail: `${thumbnail.path}.${thumbnail.extension}`,
                })
            ));

            rootElement.insertAdjacentHTML('beforeend', html);
        });
    };

    return {resetComic, loadComic};
};
