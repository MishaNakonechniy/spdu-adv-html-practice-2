import {getCardTemplate} from '/js/templates';
import {fetchComicsData} from '/js/fetch';
import {goTo} from '../utils'

export const displayComicsData = rootElement => {
    let queries = {
        limit: 20,
        page: 0,
        titleStartsWith: ''
    };

    const setComicsQueries = nextQueries => {
        const noVariants = !!nextQueries.noVariants;

        queries = {...queries, ...nextQueries, noVariants};
    };

    const resetComics = () => {
        queries.page = 0;
        rootElement.innerHTML = '';
    };

    const loadComics = () => {
        fetchComicsData(queries).then(response => {
            const html = response.data.results.map(({id, title, description, thumbnail}) => (
                getCardTemplate({
                    id,
                    name: title,
                    description: description || '',
                    thumbnail: `${thumbnail.path}.${thumbnail.extension}`,
                    onClick: (event, id) => {
                        goTo(`/comics/${id}`)
                    }
                })
            ));

            rootElement.append(...html);

            queries.page += 1;
        });
    };

    return {
        setComicsQueries,
        loadComics,
        resetComics
    };
};
