import md5 from 'md5';
import {PUBLIC_KEY, PRIVATE_KEY} from '/js/constants';

const generateCredentials = () => (
    `ts=${Date.now()}&apikey=${PUBLIC_KEY}&hash=${md5(`${Date.now()}${PRIVATE_KEY}${PUBLIC_KEY}`)}`
);

export const queryBuilder = (baseUrl, queries = []) => (
    `${baseUrl}?${queries.filter(el => !!el).join('&')}&${generateCredentials()}`
);

export const nameQuery = name => name ? `name=${encodeURIComponent(name)}` : '';

export const nameStartsWithQuery = nameStartsWith => nameStartsWith ? `nameStartsWith=${encodeURIComponent(nameStartsWith)}` : '';

export const titleQuery = title => title ? `title=${encodeURIComponent(title)}` : '';

export const titleStartsWithQuery = titleStartsWith => titleStartsWith ? `titleStartsWith=${encodeURIComponent(titleStartsWith)}` : '';

export const modifiedSinceQuery = modifiedSince => modifiedSince ? `modifiedSince=${encodeURIComponent(modifiedSince)}` : '';

export const limitQuery = limit => limit ? `limit=${encodeURIComponent(limit)}` : '';

export const orderByQuery = orderBy => orderBy ? `orderBy=${encodeURIComponent(orderBy)}` : '';

export const offsetQuery = offset => offset ? `offset=${encodeURIComponent(offset)}` : '';

export const noVariantsQuery = offset => offset ? `noVariants=${encodeURIComponent(offset)}` : '';

export const formatQuery = offset => offset ? `format=${encodeURIComponent(offset)}` : '';

export const comicsQuery = comics => comics ? `comics=${encodeURIComponent(comics)}` : '';

export const seriesQuery = series => series ? `series=${encodeURIComponent(series)}` : '';
