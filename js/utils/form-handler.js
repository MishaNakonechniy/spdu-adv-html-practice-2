export const formHandler = (form, handler) => {
    form.addEventListener('submit', event => {
        event.preventDefault();
        new FormData(form);
    });

    form.addEventListener('formdata', handler);
};
