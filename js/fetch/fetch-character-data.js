import {BASE_URL} from '/js/constants';
import {queryBuilder} from '/js/utils';

export const fetchCharacterData = id => (
    fetch(queryBuilder(`${BASE_URL}/characters/${id}`)).then(response => (
        response.json()
    ))
);
