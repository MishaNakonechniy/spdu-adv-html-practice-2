import {BASE_URL} from '/js/constants';
import {queryBuilder} from '/js/utils';

export const fetchComicData = id => (
    fetch(queryBuilder(`${BASE_URL}/comics/${id}`)).then(response => (
        response.json()
    ))
);
