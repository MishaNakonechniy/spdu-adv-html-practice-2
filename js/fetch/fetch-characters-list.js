import {BASE_URL} from '/js/constants';
import {queryBuilder, nameStartsWithQuery} from '/js/utils/query-builder';

export const fetchCharactersList = ({nameStartsWith}) => (
    fetch(queryBuilder(`${BASE_URL}/characters`, [nameStartsWithQuery(nameStartsWith)])).then(response => (
        response.json()
    ))
);
