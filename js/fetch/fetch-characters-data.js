import {BASE_URL} from '/js/constants';
import {
    nameQuery,
    nameStartsWithQuery,
    modifiedSinceQuery,
    limitQuery,
    orderByQuery,
    queryBuilder,
    offsetQuery,
    comicsQuery,
    seriesQuery
} from '/js/utils/query-builder';

export const fetchCharactersData = queries => {
     const {name, nameStartsWith, modifiedSince, limit, orderBy, order, page, comics, series} = queries;
     const ascendingOrder = orderBy && order === 'ASC' ? orderBy : '';
     const descendingOrder = orderBy && order === 'DSC' ? `-${orderBy}` : '';

     const queriesArray = [
         nameQuery(name),
         nameStartsWithQuery(nameStartsWith),
         modifiedSinceQuery(modifiedSince),
         limitQuery(limit),
         orderByQuery(ascendingOrder || descendingOrder),
         offsetQuery(limit * page),
         comicsQuery(comics),
         seriesQuery(series)
     ];

    return fetch(queryBuilder(`${BASE_URL}/characters`, queriesArray)).then(response => (
        response.json()
    ));
};
