import {addRoutes, goTo, formHandler, datalistHandler} from './utils';
import {displayCharactersData, displayCharacterData, displayComicsData, displayComicData} from './display';
import {fetchCharactersList} from './fetch';

const ROOT = document.getElementById('root');
const CHARACTERS_FORM = document.getElementById('characters-form');
const CHARACTERS_FORM_DATALIST = document.getElementById('characters-form-datalist');
const COMICS_FORM = document.getElementById('comics-form');
const LOAD_MORE_BUTTON = document.getElementById('load-more-button');

const CHARACTER_NAME_INPUT = document.querySelector('[name="name"]');

const {loadCharacters, setCharactersQueries, resetCharacters} = displayCharactersData(ROOT);
const {loadCharacter, resetCharacter} = displayCharacterData(ROOT);
const {loadComics, setComicsQueries, resetComics} = displayComicsData(ROOT);
const {loadComic, resetComic} = displayComicData(ROOT);
const {
    addOptions: addCharactersOptions,
    removeOptions: removeCharactersOptions
} = datalistHandler(CHARACTERS_FORM_DATALIST);

const noop = () => {};

const loadMoreRef = {handler: noop};

LOAD_MORE_BUTTON.addEventListener('click', () => {
    loadMoreRef.handler();
});

// Routes order is important (from more specific to less specific)!

addRoutes([
    {
        path: '/comics/:id',
        handler: ({id}) => {
            resetComic();
            loadComic(id);
            loadMoreRef.handler = noop;
        }
    },
    {
        path: '/comics',
        handler: () => {
            resetComics();
            loadComics();
            loadMoreRef.handler = loadComics;
        }
    },
    {
        path: '/:id',
        handler: ({id}) => {
            resetCharacter();
            loadCharacter(id);
            loadMoreRef.handler = noop;
        }
    },
    {
        path: '/',
        handler: () => {
            resetCharacters();
            loadCharacters();
            loadMoreRef.handler = loadCharacters;
        }
    }
]);

formHandler(COMICS_FORM, event => {
    setComicsQueries(Object.fromEntries(event.formData));
    goTo('/comics');
});

formHandler(CHARACTERS_FORM, event => {
    setCharactersQueries(Object.fromEntries(event.formData));
    goTo('/');
});

CHARACTER_NAME_INPUT.addEventListener('focus', () => {
    removeCharactersOptions();
});

CHARACTER_NAME_INPUT.addEventListener('input', event => {
    fetchCharactersList({nameStartsWith: event.currentTarget.value}).then(response => {
        const options = response.data.results.map(({name}) => name);

        removeCharactersOptions();
        addCharactersOptions(options);
    });
});
